(function (_, jQuery) {

    let dashboardFav = document.querySelectorAll('.dashboard-fav-click');
    [...dashboardFav].forEach(el => el.addEventListener('click', event => {
        var dashboardId = event.target.id;
        var isFavourite;
        var dashboardFavEle = document.getElementById(`${dashboardId}`);
        var dashboardFavClickEle = document.getElementById(`${dashboardId}-fav-click`);
        var dashboardFavInfo = document.getElementById(`${dashboardId}-fav-info`);

        if (dashboardFavEle.classList.contains("fa-star")) {
            isFavourite = false;
            favUnfavDashboard(dashboardId, isFavourite, function(res) {
                if (res) {
                    dashboardFavClickEle.classList.remove('dashboard-fav-active')
                    setTimeout(function() {
                        dashboardFavClickEle.classList.remove('dashboard-fav-active-2')
                    }, 30)
                    dashboardFavClickEle.classList.remove('dashboard-fav-active-3')
                    setTimeout(function() {
                        dashboardFavEle.classList.remove('fa-star')
                        dashboardFavEle.classList.add('fa-star-o')
                    }, 15)
                }
            }, function(err){
                console.log("Not able to unfavoured dashboard")
            })
        } else {
            isFavourite = true;
            favUnfavDashboard(dashboardId, isFavourite, function(res) {
                    if (res) {
                        dashboardFavClickEle.classList.add('dashboard-fav-active')
                        dashboardFavClickEle.classList.add('dashboard-fav-active-2')
                        setTimeout(function() {
                            dashboardFavEle.classList.add('fa-star')
                            dashboardFavEle.classList.remove('fa-star-o')
                        }, 150)
                        setTimeout(function() {
                            dashboardFavClickEle.classList.add('dashboard-fav-active-3')
                        }, 150)
                        dashboardFavInfo.classList.add('dashboard-fav-info-tog')

                        setTimeout(function(){
                            dashboardFavInfo.classList.remove('dashboard-fav-info-tog')

                        },1000)
                    }
             }, function(err){
                    console.log("Not able to favourite dashboard")
                })
        }

    }));


    var favUnfavDashboard = function(dashboardId, isFavourite, success, error) {
                    $.ajax({
                        'url': '/api/3/action/make_fav_unfav_dashboard',
                        'method': 'POST',
                        'data': {
                            'dashboard_id': dashboardId,
                            'is_favourite': isFavourite
                        },
                        'dataType': 'json',
                        'success': function(resp){
                            if (success){
                                success(resp.result)
                            }
                        },
                        'error': error
                    })
                }


    })(ckan.i18n.ngettext, $);